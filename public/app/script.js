var tripperApp = angular
    .module('tripperApp', ['ngRoute'])
    .config(['$locationProvider', function ($locationProvider) {
        $locationProvider.hashPrefix('');
    }]);

tripperApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'app/pages/landing.html',
            controller: 'mainController'
        })

        .when('/register', {
            templateUrl: 'app/pages/register.html',
            controller: 'registerController'
        })

        // .when('/login', {
        //     templateUrl: 'app/pages/login.html',
        //     controller: 'loginController'
        // })

        .when('/addTrip', {
            templateUrl: 'app/pages/addTrip.html'
        })

        .when('/mytrips', {
            templateUrl: 'app/pages/mytrips.html',
            controller: 'myTripsController'
        })

        .when('/trip/:tripId', {
            templateUrl: 'app/pages/edit.html',
            controller: 'tripController'
        })

        .otherwise({ redirectTo: '/' });
});

tripperApp.directive("compareTo", function () {
    return {
        require: "ngModel",
        scope:
        {
            confirmPassword: "=compareTo"
        },
        link: function (scope, element, attributes, modelVal) {
            modelVal.$validators.compareTo = function (val) {
                return val == scope.confirmPassword;
            };
            scope.$watch("confirmPassword", function () {
                modelVal.$validate();
            });
        }
    };
});

tripperApp.directive('modal', function () {
    return {
        template: '<div class="modal fade">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title">{{ title }}</h4>' +
        '</div>' +
        '<div class="modal-body" ng-transclude></div>' +
        '</div>' +
        '</div>' +
        '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

tripperApp.controller('mainController', function ($scope, $timeout, $location, $http) {
    $scope.message = 'Landing page bla bla...';
    $scope.isSignedIn = sessionStorage.loginData ? !!JSON.parse(sessionStorage.loginData).access_token : false;
    $scope.showModal = false;
    $scope.user = { username: null, password: null };

    $scope.toggleModal = function () {
        $scope.showModal = !$scope.showModal;
    };

    $scope.signIn = function () {

        /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
        //$scope.loginDataLoading = true;

        //$timeout(function () {
        //    if ($scope.user.username === 'test' && $scope.user.password === 'test') {
        //        $scope.isSignedIn = true;
        //        $scope.showError = false;
        //        $scope.showModal = !$scope.showModal;
        //        $location.path('/mytrips');
        //    }
        //    else {
        //        $scope.errorMessage = 'Username or password is incorrect';
        //        $scope.showError = true;
        //    }
        //    $scope.loginDataLoading = false;
        //}, 1000);


        /* Use this for real authentication
         ----------------------------------------------*/
        $scope.loginDataLoading = true;

        var grant_type = 'password';
        var username = $scope.user.username;
        var password = $scope.user.password;
        var postData = 'grant_type=' + grant_type + '&username=' + username + '&password=' + password;

        $http.post('http://tripper-api.azurewebsites.net/token', postData)
            .then(function (response) {
                console.log('success', response);

                sessionStorage.loginData = JSON.stringify(response.data);
                sessionStorage.loggedUser = JSON.stringify($scope.user);
                $scope.showError = false;
                $scope.toggleModal();
                $scope.isSignedIn = true;

                $location.path('/mytrips');
                $scope.loginDataLoading = false;

            }, function errorCallback(response) {
                console.log('error', response);

                $scope.errorMessage = response.data.error_description;
                if ($scope.errorMessage === null || $scope.errorMessage === undefined) {
                    $scope.errorMessage = 'Connection error occurred.';
                }
                console.log(response.data.error_description);
                $scope.showError = true;
                $scope.loginDataLoading = false;
            });
    };

    $scope.signOut = function () {
        sessionStorage.removeItem('loginData');
        sessionStorage.removeItem('loggedUser');
        $scope.isSignedIn = false;
    };
});

tripperApp.controller('tripController', ['$scope', '$http', '$location', '$routeParams', function ($scope, $http, $location, $routeParams) {
    $scope.deleteTrip = function () {
        $http.delete('http://tripper-api.azurewebsites.net/trips/' + $routeParams.tripId).then(function (res) {
            $location.path('/mytrips');
        }, function (res) {
            console.log(res);
        });
    }
}]);

// tripperApp.controller('loginController', function ($scope) {
//     $scope.message = 'Login page bla bla...';
// });

tripperApp.controller('registerController', function ($scope, $http, $location, $timeout) {
    if ($scope.isSignedIn) {
        $location.path('/');
    }

    $scope.message = 'Register page bla bla...';

    $scope.showRegisterError = false;
    $scope.showRegisterSuccess = false;

    $scope.registerUser = { registerUsername: null, registerPassword: null, confirmRegisterPassword: null, email: null };

    $scope.register = function () {

        $scope.showRegisterError = false;
        $scope.showRegisterSuccess = false;
        $scope.registerDataLoading = true;

        var registerData = {
            login: $scope.registerUser.registerUsername,
            password: $scope.registerUser.registerPassword,
            passwordConfirmation: $scope.registerUser.confirmRegisterPassword,
            email: $scope.registerUser.email
        };

        $http.post('http://tripper-api.azurewebsites.net/register', registerData)
            .then(function (response) {
                if (response.data.result === 'success') {
                    $scope.showRegisterSuccess = true;
                    $timeout(function(){
                        $location.path('/');
                    }, 600);
                }
                else {
                    if (response.data.result === 'fail')
                        $scope.registerErrorMessage = response.data.error_message;
                    else
                        $scope.registerErrorMessage = 'Registration service works, but sends unsupported data.';
                    $scope.showRegisterError = true;
                }

                $scope.registerDataLoading = false;

            }, function errorCallback(response) {
                $scope.registerErrorMessage = response.data.error_description;
                if ($scope.registerErrorMessage === null || $scope.registerErrorMessage === undefined) {
                    $scope.registerErrorMessage = 'Connection error occurred.';
                }
                $scope.showRegisterError = true;

                $scope.registerDataLoading = false;

            });
    };
});

tripperApp.controller('myTripsController', function ($scope, $http, $location) {
    // if (!sessionStorage.token) {
    //     $location.path('/');
    //     $scope.toggleModal();
    // }
    // else {
    $scope.message = 'Here you can see all your recorded trips!';
    $http.get("http://tripper-api.azurewebsites.net/trips", {
        headers: {'authorization' : JSON.parse(sessionStorage.loginData).token_type + ' ' + JSON.parse(sessionStorage.loginData).access_token}
  })
        .then(function (response) {
            if (!response.data.errorMessages) {
                $scope.myObj = response.data.trips;

                var ids = response.data.trips.map(function (value) {
                    return value.id;
                });
                $scope.posters = {};
                $scope.presentations = {};
                $scope.posterIds = ids;
                $scope.presentationIds = ids;
              ids.forEach(function(el){
                $http({
                    url: "http://tripper-api.azurewebsites.net/trips/" + el +"/posters",
                    method: "GET",
                    tripId: el
                }).then($scope.posterSuccessHandler)
                    .catch(function (err) {
                        console.log(err);
                    });

               $http({
                    url: "http://tripper-api.azurewebsites.net/trips/" + el +"/presentations",
                    method: "GET",
                    tripId: el
                }).then($scope.presentationSuccessHandler)
                    .catch(function (err) {
                        console.log(err);
                    });

              });
              setTimeout(function(){
                  $scope.presentationQueue();
                  $scope.posterQueue();
              }, 10000);

            } else {
                $scope.myObj = { trips: [] };
            }
        })
        .catch(function (err) {
            console.log(err);
        });
    $scope.presentationSuccessHandler = function (response) {
        var tripId = response.config.tripId;
        var keys = Object.keys(response.data);
        var isGood = false;
        keys.forEach(function(key){
            if(response.data[key].result === 'fail') {

            } else {
                isGood = true;
                var index = $scope.posterIds.indexOf(tripId);
                if (index > -1) {
                    $scope.posterIds.splice(index, 1);
                }
                $scope.presentations[tripId] = response.data[key];
            }
        });
    };

    $scope.posterSuccessHandler = function (response) {
        var tripId = response.config.tripId;
        if(response.data[""] && response.data[""].result === 'fail') {

        } else {
            var index = $scope.posterIds.indexOf(tripId);
            if (index > -1) {
                $scope.posterIds.splice(index, 1);
            }
            var keys = Object.keys(response.data);
            $scope.posters[tripId] = response.data[keys[keys.length-1]];
        }
    };

    $scope.presentationQueue = function() {
        if($scope.presentationIds.length > 0) {
            $scope.presentationIds.forEach(function(el){
                $http({
                    url: "http://tripper-api.azurewebsites.net/trips/" + el +"/presentations",
                    method: "GET",
                    tripId: el
                }).then($scope.presentationSuccessHandler)
                    .catch(function (err) {
                        console.log(err);
                    });
            });
            setTimeout(function(){
                $scope.presentationQueue();
            }, 200000)
        }
    };

    $scope.posterQueue = function() {
        if($scope.posterIds.length > 0) {
            $scope.posterIds.forEach(function(el){
                $http({
                    url: "http://tripper-api.azurewebsites.net/trips/" + el +"/posters",
                    method: "GET",
                    tripId: el
                }).then($scope.posterSuccessHandler)
                    .catch(function (err) {
                        console.log(err);
                    });
            });
            setTimeout(function(){
                $scope.posterQueue();
            }, 100000)
        }
    };
    $scope.mediaCallHandler = function (response, url, onSuccess) {
        var counter = 0;
        var inProgress = [];
        for (var el in response.data) {
            if (response.data[el].result === "fail") {
                counter++;
                inProgress.push(el);
            }
        }
        if (counter > 0) {
            setTimeout(function () {
                $http({
                    url: url,
                    method: "GET",
                    params: { id_list: inProgress }
                }).then($scope[onSuccess])
                    .catch(function (err) {
                        console.log(err);
                    });
            }, 10000);
        }
    }

    $scope.edit = function (id) {
        var earl = '/trip/' + id;
        $location.path(earl);
    };

    $scope.showVideo = function (id, link) {
        // $("#myModal").modal('show');
        // $('#myModal iframe').attr('width', 420);
        // $('#myModal iframe').attr('height', 315);
        // $('#myModal iframe').attr('src', link);
         window.open(link, '_blank');
    }

    $scope.showPoster = function (id, link) {
        // $("#myModal").modal('show');
        // $('#myModal iframe').attr('width', 800);
        // $('#myModal iframe').attr('height', 600);
        // $('#myModal iframe').attr('src', link);
        window.open(link, '_blank');
    }
    // }
});