var tripId = window.location.href.split('/').pop();

var map;

var posterLink, presLink;

var markerUrls = [];

var customUrls = [];

var editTripContent = $('#editTripContent');

$.ajax({
	url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/media',
	success: function(data){
		for(var i = 0; i < data.length; i++){
			if(data[i].m_id)
				markerUrls.push({ url: data[i].url, m_id: data[i].m_id });
			else if(data[i].lat)
				customUrls.push({ url: data[i].url, lat: data[i].lat, lng: data[i].lng });
		}

		console.log(markerUrls);
		console.log(customUrls);
	},
	error: function(data){
		console.log('server media error');
	},
	complete: function(){
		$.ajax({
			url: 'http://tripper-api.azurewebsites.net/trips/' + tripId,
			success: function(data){
				// console.log(data);
				$('h1').text('Edit trip');

				var tripData = JSON.parse(data.tripData);
				var features = tripData.features;

				if(data.name)
					$('#editTripContent ul').append('<li class="list-group-item"><div class="input-group"><span class="input-group-addon">Name</span><input type="text" class="form-control" value="'+data.name+'"></div></li>');
				
				if(data.description)
					$('#editTripContent ul').append('<li class="list-group-item"><div class="input-group"><span class="input-group-addon">Description</span><input type="text" class="form-control" value="'+data.description+'"></div></li>');
				
				if(data.created)
					$('#editTripContent ul').append('<li class="list-group-item"><div class="input-group"><span class="input-group-addon">Created</span><input type="text" class="form-control" value="'+data.created.substr(0, 19).replace('T', ' ')+'" disabled></div></li>');

				editTripContent.slideDown('fast');

				var flen = features.length;
				var idx = 0;
				for(var i = 0; i < flen; i++){
					if(features[i].geometry.type == 'LineString'){
						if(i == 0)
							map = new GMaps({
								el: '#map',
								lat: features[0].geometry.coordinates[0][1],
								lng: features[0].geometry.coordinates[0][0],
								zoom: 10
							});
						var clen = features[i].geometry.coordinates.length;
						for(var j = 0; j < clen; j++){
							map.addMarker({
								lat: features[i].geometry.coordinates[j][1],
								lng: features[i].geometry.coordinates[j][0],
								m_id: idx,
								infoWindow: {
									content: '<p>point ' + idx++ + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
								}
							});
						}
					}
					if(features[i].geometry.type == 'Point'){
						if(i == 0)
							map = new GMaps({
								el: '#map',
								lat: features[0].geometry.coordinates[1],
								lng: features[0].geometry.coordinates[0],
								zoom: 10
							});
						map.addMarker({
							lat: features[i].geometry.coordinates[1],
							lng: features[i].geometry.coordinates[0],
							m_id: idx,
							infoWindow: {
								content: '<p>point ' + idx++ + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
							}
						});
					}
					if(features[i].geometry.type == 'MultiLineString'){
						if(i == 0)
							map = new GMaps({
								el: '#map',
								lat: features[0].geometry.coordinates[0][0][1],
								lng: features[0].geometry.coordinates[0][0][0],
								zoom: 10
							});
						var clen = features[i].geometry.coordinates.length;
						for(var j = 0; j < clen; j++){
							var zlen = features[i].geometry.coordinates[j].length;
							for(var z = 0; z < zlen; z++){
								map.addMarker({
									lat: features[i].geometry.coordinates[j][z][1],
									lng: features[i].geometry.coordinates[j][z][0],
									m_id: idx,
									infoWindow: {
										content: '<p>point ' + idx++ + '<br>select image: <input type="file" id="tripImg" name="tripImg" /></p>'
									}
								});
							}
						}
					}

					if(features[i].properties.name){
						if(features[i].properties.desc)
							$('#placesTable').append('<tr><td>'+ i + '</td><td>' + features[i].properties.name + '</td><td>' + features[i].properties.desc + '</td></tr>');
						else
							$('#placesTable').append('<tr><td>' + i + '</td><td>' + features[i].properties.name + '<td></td></tr>');	
					}
						
				}
			},
			error: function(data){
				$('#errMsg').append('<div class="alert alert-dismissible alert-danger">Error while loading trip</div>');
			},
			complete: function(){
				var path = [];
				var len = markerUrls.length;
				for(var i = 0; i < map.markers.length; i++){
					if(len > 0){
						for(var j = 0; j < len; j++)
							if(map.markers[i].m_id == markerUrls[j].m_id){
								console.log('photo with m_id: ' + map.markers[i].m_id + ' added');
								map.markers[i].infoWindow.setContent('<p>point ' + i + '<br><img src=' + markerUrls[j].url + ' height="100" width="100"><br>select image: <input type="file" id="tripImg" name="tripImg" /></p>');
							}	
					}

					path.push([map.markers[i].position.lat(), map.markers[i].position.lng()]);

					map.markers[i].addListener('click', function(){
						// var lat = this.getPosition().lat();
						// var lng = this.getPosition().lng();
						var id = this.get('m_id');
						console.log('marker id: ' + id);
						$('#tripImg').change(function(){
							var fd = new FormData();    
							fd.append('mediaFile', $(this)[0].files[0]);
							$.ajax({
								url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/media?m_id=' + id,
								data: fd,
								processData: false,
								contentType: false,
								type: 'POST',
								success: function(data){
									$('#errMsg div').remove();
									$('#successMsg div').remove();
									$('#successMsg').append('<div class="alert alert-dismissible alert-success">Image saved</div>');
								},
								error: function(data){
									$('#errMsg div').remove();
									$('#successMsg div').remove();
									$('#errMsg').append('<div class="alert alert-dismissible alert-danger">Server side error</div>');
								}
							});
						});
					});
				}

				map.drawPolyline({
					path: path,
					strokeColor: '#131540',
					strokeOpacity: 0.6,
					strokeWeight: 6
				});

				cidx = 0;

				var lenC = customUrls.length;

				if(lenC > 0){
					for(var i = 0; i < lenC; i++)
						map.addMarker({
							lat: customUrls[i].lat,
							lng: customUrls[i].lng,
							icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
							infoWindow: {
								content: '<p>custom point ' + cidx + '<br><img src=' + customUrls[i].url + ' height="100" width="100"><br><input type="button" value="remove" onclick="deleteMarker(' + cidx++ + ')" /></p>'
							}
						});
				}

				GMaps.on('click', map.map, function(evt){
					var lat = evt.latLng.lat();
			    	var lng = evt.latLng.lng();
			    	map.addMarker({
			    		lat: lat,
			    		lng: lng,
			    		cidx: cidx,
			    		icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
			    		infoWindow: {
			    			content: '<p>custom point: ' + cidx + '<br>select image: <input type="file" id="customImg" name="customImg" /><br><input type="button" value="remove" onclick="deleteMarker(' + cidx++ + ')" /></p>'
			    		}
			    	});

					map.markers[map.markers.length - 1].addListener('click', function(){
						$('#customImg').change(function(){
							var fd = new FormData();    
							fd.append('mediaFile', $(this)[0].files[0]);
							$.ajax({
								url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/media2?lat=' + lat + '&lng=' + lng,
								data: fd,
								processData: false,
								contentType: false,
								type: 'POST',
								success: function(data){
									console.log(data);
									$('#errMsg div').remove();
									$('#successMsg div').remove();
									$('#successMsg').append('<div class="alert alert-dismissible alert-success">Image saved</div>');

								},
								error: function(data){
									console.log(data);
									$('#errMsg div').remove();
									$('#successMsg div').remove();
									$('#errMsg').append('<div class="alert alert-dismissible alert-danger">Server side error</div>');
								}
							});

						});
					});
				});
			}
		});
	}
});
function posterCall() {
  $.ajax({
    url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/posters',
    success: function (data) {
    	if(data && data[""] && data[""].result === 'fail') {

			} else {
        var keys = Object.keys(data);
        posterLink = data[keys[keys.length - 1]].link;
        $('#showPosterBtn').show();
      }
    },
    error: function (data) {
      console.log('server media error');
    }});
}

posterCall();

function presentationCall() {
  $.ajax({
    url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/presentations',
    success: function (data) {
    	 var keys = Object.keys(data);
        keys.forEach(function(key){
        	if(data[key].result === 'fail') {

          } else {
        		presLink = data[key].link;
        		$('#showPresBtn').show();
        	}
        });
    },
    error: function (data) {
      console.log('server media error');
    }});
}

presentationCall();

function generatePoster() {
	$.ajax({
		type: 'POST',
		url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/run_job/poster',
		success: function(){
			alert("Generation Started.");
			posterCall();
		},
		error: function(){
			console.log('server media error');
		}
});

}

function generatePresentation() {
	$.ajax({
		type: 'POST',
	url: 'http://tripper-api.azurewebsites.net/trips/' + tripId + '/run_job/presentation',
	success: function(){
			alert("Generation Started.");
			presentationCall();
	},
	error: function(){
		console.log('server media error');
	}
});
}

var showVideo = function (id, link) {
		window.open(presLink, '_blank');
}

var showPoster = function (id, link) {
		window.open(posterLink, '_blank');
}

function deleteMarker(cidx){
	var len = map.markers.length;
	var tidx;
	while(len--){
		tidx = map.markers[len].get('cidx');
		if(tidx == cidx){
			map.markers[len].setMap(null);
			break;
		}
	}
}

