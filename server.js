var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req,res) {
    res.sendFile(path.join(__dirname + '/public/app/index.html'));
});


app.listen(port, function(){
	console.log('Running the server on port ' + port);
});